const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const GroupSchema = new mongoose.Schema({
  group_name: {
    type: String,
    required: true,
  },
  group_admin: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  group_members: [
    {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
  ],
});

module.exports = mongoose.model("Group", GroupSchema);
