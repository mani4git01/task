const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const MessageSchema = new mongoose.Schema({
  group: {
    type: Schema.Types.ObjectId,
    ref: "Group",
    required: true,
  },
  writer: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  liked_by: [
    {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
  ],
  message: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Message", MessageSchema);
