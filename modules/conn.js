const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const uri = "mongodb+srv://"+process.env.MDB_USER+":"+process.env.MDB_PASS+"@"+process.env.MDB_HOST
const connect = mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
conn=mongoose.connection
module.exports=conn