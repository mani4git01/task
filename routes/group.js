var express = require("express");
var router = express.Router();
const Group = require("../models/group");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const Message = require("../models/message");
const { body, validationResult } = require("express-validator");

/* GET Groups listing. */
router.get("/", function (req, res, next) {
  res.send("Welcome");
});

router.get("/search", async (req, res) => {
  const groups = await Group.find();
  if (groups.length != 0) res.json({ status: true, groups: groups });
  else res.json({ status: false, error: "Not Found" });
});

router.post(
  "/create",
  body("group_name").isLength({ min: 5 }),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    let decoded = {};
    let groups = {};
    let id = {};
    try {
      token = req.headers.authorization.split(" ")[1];

      decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
    } catch (err) {
      return res.status(400).send({
        status: false,
        message: "Unauthorized User",
      });
    }
    try {
      const name = req.body.group_name;
      groups = await Group.findOne({ group_name: name });
    } catch (err) {
      return res.status(400).send({
        status: false,
        message: "Error Occurred",
        error: err,
      });
    }
    if (!groups) {
      try {
        const new_group = await Group({
          group_name: req.body.group_name,
          group_admin: req.body.group_admin,
          group_members: req.body.group_members,
        });
        await new_group.save();
        id = new_group._id;
      } catch (err) {
        return res.status(400).send({
          status: false,
          message: "Invalid members ID",
          error: err,
        });
      }
      return res.send({
        status: true,
        message: "Group is Successfully Created",
        _id: id,
      });
    } else {
      return res.status(400).send({
        status: false,
        message: "Group Name is already taken",
      });
    }
  }
);

router.post(
  "/add",
  body("group_id").not().isEmpty(),
  body("new_id").not().isEmpty(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    let decoded = {};
    let groups = {};
    try {
      token = req.headers.authorization.split(" ")[1];

      decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
    } catch (err) {
      return res.status(400).send({
        status: false,
        message: "Unauthorized User",
      });
    }
    try {
      const id = req.body.group_id;
      groups = await Group.findById(id);
      let group_mems = groups.group_members;
      if (group_mems.includes(req.body.new_id)) {
        return res.send({
          status: false,
          message: "User already in group",
        });
      }
      group_mems.push(req.body.new_id);
      groups.group_members = group_mems;
      await groups.save();
      return res.send({
        status: true,
        message: "User added Successfully",
      });
    } catch (err) {
      return res.status(400).send({
        status: false,
        message: "Error Occurred",
        error: err,
      });
    }
  }
);

router.post(
  "/remove",
  body("group_id").not().isEmpty(),
  body("old_id").not().isEmpty(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    let decoded = {};
    let groups = {};
    try {
      token = req.headers.authorization.split(" ")[1];

      decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
    } catch (err) {
      return res.status(400).send({
        status: false,
        message: "Unauthorized User",
      });
    }
    try {
      const id = req.body.group_id;
      groups = await Group.findById(id);
      let group_mems = groups.group_members;
      if (!group_mems.includes(req.body.old_id)) {
        return res.send({
          status: false,
          message: "User not in group",
        });
      }
      group_mems.splice(group_mems.indexOf(req.body.old_id), 1);
      groups.group_members = group_mems;
      await groups.save();
      return res.send({
        status: true,
        message: "User Removed Successfully",
      });
    } catch (err) {
      return res.status(400).send({
        status: false,
        message: "Error Occurred",
        error: err,
      });
    }
  }
);

router.get("/:name", async (req, res) => {
  const groups = await Group.find({ group_name: req.params.name });
  if (groups.length != 0) res.json(groups);
  else res.json({ error: "Not Found" });
});

router.delete("/:id", async (req, res) => {
  let decoded = {};
  let groups = {};
  try {
    token = req.headers.authorization.split(" ")[1];

    decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
  } catch (err) {
    return res.status(400).send({
      status: false,
      message: "Unauthorized User",
    });
  }
  let x = false;
  try {
    const group = await Group.findById(req.params.id);
    const messages = await Message.find({ group: req.params.group_id });
    x = group;
    for (let m of messages) {
      m.remove();
    }
  } catch (err) {
    return res.status(400).send("Invalid ID");
  }
  if (!x) res.status("404").send("Error");
  else {
    await x.remove();
    res.send({
      status: true,
      message: "Group removed Successfully",
    });
  }
});

module.exports = router;
