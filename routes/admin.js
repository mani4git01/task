var express = require("express");
var router = express.Router();
const User = require("../models/user");
const jsonwebtoken = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { body, validationResult } = require("express-validator");

router.get("/search", async (req, res) => {
  const users = await User.find().select("user_name is_admin");
  if (users.length != 0) res.json({ status: true, users: users });
  else res.json({ error: "Not Found" });
});

router.post(
  "/add",
  body("user_name").isLength({ min: 5 }),
  body("password").isLength({ min: 5 }),
  body("is_admin").isBoolean(),
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    let token = req.headers.authorization.split(" ")[1];
    try {
      let decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
      if (decoded.is_admin) {
        var password = req.body.password;
        let user = User({
          user_name: req.body.user_name,
          password: bcrypt.hashSync(password, parseInt(process.env.SALTROUNDS)),
          is_admin: req.body.is_admin,
        });
        user.save();
        res.send({
          status: true,
          _id: user._id,
          message: "User Created Successfully",
        });
      } else {
        res.status(400).send({
          status: false,
          message: "Unauthorized User",
        });
      }
    } catch (err) {
      res.status(400).send({
        status: false,
        message: "Token Expired",
        error: err,
      });
    }
  }
);

router.put(
  "/:id",
  body("user_name").isLength({ min: 5 }),
  body("is_admin").isBoolean(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    token = req.headers.authorization.split(" ")[1];
    try {
      let decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
      if (decoded.is_admin) {
        user = await User.findOneAndUpdate(
          { _id: req.params.id },
          {
            user_name: req.body.user_name,
            is_admin: req.body.is_admin,
          }
        );
        user = await User.find({ _id: req.params.id }).select(
          "user_name is_admin"
        );
        res.send({
          status: true,
          message: "User Updated Successfully",
          updated_user: user,
        });
      } else {
        res.status(400).send({
          status: false,
          message: "Unauthorized User",
        });
      }
    } catch (err) {
      res.status(400).send({
        status: false,
        message: "Token Expired",
        error: err,
      });
    }
  }
);

router.delete("/:id", async (req, res) => {
  token = req.headers.authorization.split(" ")[1];
  try {
    let decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
    if (decoded.is_admin) {
      let x = {};
      try {
        const user = await User.findById(req.params.id);
        x = user;
      } catch (err) {
        return res.status(400).send("Invalid ID");
      }
      if (!x)
        return res
          .status("404")
          .send({ status: false, message: "Id not found" });
      else {
        await x.remove();
        return res.send({ status: true, message: "Success.." });
      }
    }
  } catch (err) {
    return res.status("404").send({ status: false, message: "Error.." });
  }
});

module.exports = router;
