var express = require("express");
var router = express.Router();
const Message = require("../models/message");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const User = require("../models/user");
const Group = require("../models/group");
const { body, validationResult } = require("express-validator");

/* GET messages listing. */
router.get("/", function (req, res, next) {
  res.send("Welcome....MSG");
});

router.get("/search", async (req, res) => {
  const messages = await Message.find();
  if (messages) {
    return res.json({ status: true, messages: messages });
  } else {
    return res.json({ status: false, error: "Not Found" });
  }
});

router.get("/search/:id", async (req, res) => {
  const messages = await Message.findById(req.params.id);
  if (messages) {
    res.json({ status: true, messages: messages });
  } else res.json({ status: false, message: "Not Found" });
});

router.get("/:group_id", async (req, res) => {
  const messages = await Message.find({ group: req.params.group_id });
  if (messages) {
    res.json({ status: true, messages: messages });
  } else res.json({ status: false, error: "Not Found" });
});

router.post("/send", body("group_id").not().isEmpty(), async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  let decoded = {};
  let groups = {};
  try {
    token = req.headers.authorization.split(" ")[1];

    decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
  } catch (err) {
    return res.status(400).send({
      status: false,
      message: "Unauthorized User",
    });
  }
  const user = await User.findById(decoded._id);
  if (user) {
    const group = await Group.findById(req.body.group_id);
    if (group && group.group_members.includes(decoded._id)) {
      const message = Message({
        group: req.body.group_id,
        writer: user._id,
        liked_by: [],
        message: req.body.message,
      });
      await message.save();
      return res.send({
        status: true,
        message: "Message sent Successfully",
        _id: message._id,
      });
    } else if (group) {
      return res.send({
        status: false,
        message: "User not in the group",
      });
    } else {
      return res.send({
        status: false,
        message: "Group Not found",
      });
    }
  } else {
    return res.send({
      status: false,
      message: "Unauthorized User",
    });
  }
});

router.post("/like", body("message_id").not().isEmpty(), async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  let decoded = {};
  let groups = {};
  try {
    token = req.headers.authorization.split(" ")[1];

    decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
  } catch (err) {
    return res.status(400).send({
      status: false,
      message: "Unauthorized User",
    });
  }
  const message = await Message.findById(req.body.message_id);
  if (message) {
    const group = await Group.findById(message.group);
    if (group && group.group_members.includes(decoded._id)) {
      if (!message.liked_by.includes(decoded._id)) {
        message.liked_by.push(decoded._id);
        await message.save();
        return res.send({
          status: true,
          message: "Liked Successfully..",
        });
      } else {
        return res.send({
          status: false,
          message: "User Already Liked",
        });
      }
    } else {
      return res.send({
        status: false,
        message: "User Not in Same group",
      });
    }
  } else {
    return res.send({
      status: false,
      message: "Message Not found",
    });
  }
});

router.delete("/:id", async (req, res) => {
  let decoded = {};
  let groups = {};
  try {
    let token = req.headers.authorization.split(" ")[1];

    decoded = jsonwebtoken.verify(token, process.env.SECRET).data;
  } catch (err) {
    return res.status(400).send({
      status: false,
      message: "Unauthorized User",
    });
  }
  try {
    const message = await Message.findById(req.params.id);
    if (message && message.writer == decoded._id) {
      await message.remove();
      return res.send({
        status: true,
        message: "Message removed Successfully",
      });
    } else if (message) {
      return res.status(404).send({
        status: false,
        message: "User Not the sender",
      });
    } else {
      return res.status(404).send({
        status: false,
        message: "Message not found",
      });
    }
  } catch (err) {
    return res.status(400).send({
      status: false,
      message: "Invalid ID",
    });
  }
});

module.exports = router;
