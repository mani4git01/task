var express = require("express");
var router = express.Router();
const User = require("../models/user");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const { body, validationResult } = require("express-validator");

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("welcome");
});

router.get("/search", async (req, res) => {
  const users = await User.find().select("user_name is_admin");
  if (users.length != 0) res.json({ status: true, users: users });
  else res.json({ error: "Not Found" });
});

router.post(
  "/login",
  body("user_name").isLength({ min: 5 }),
  body("password").isLength({ min: 5 }),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const name = req.body.user_name;
      const users = await User.findOne({ user_name: name });
      if (bcrypt.compareSync(req.body.password, users.password)) {
        return res.send({
          status: true,
          _id: users._id,
          is_admin: users.is_admin,
          user_name: name,
          token: jsonwebtoken.sign(
            {
              data: {
                _id: users._id,
                user_name: name,
                password: users.password,
                is_admin: users.is_admin,
              },
            },
            process.env.SECRET,
            { expiresIn: "1d" }
          ),
        });
      } else {
        return res.send({
          status: false,
          message: "Invalid password",
        });
      }
    } catch (err) {
      return res.status(400).send({
        status: false,
        message: "Invalid User Name",
      });
    }
  }
);

router.post("/add", async (req, res) => {
  var password = req.body.password;
  let user = User({
    user_name: req.body.user_name,
    password: bcrypt.hashSync(password, process.env.SALTROUNDS),
    is_admin: req.body.is_admin,
  });
  user.save();
  res.send("Success");
});

router.get("/:name", async (req, res) => {
  const users = await User.find({ user_name: req.params.name }).select(
    "user_name is_admin"
  );
  if (users.length != 0) res.json(users);
  else res.json({ error: "Not Found" });
});

router.delete("/:id", async (req, res) => {
  let x = false;
  try {
    const user = await User.findById(req.params.id);
    x = user;
  } catch (err) {
    return res.status(400).send("Invalid ID");
  }
  if (!x) res.status("404").send("Error");
  else {
    await x.remove();
    res.send("Success..");
  }
});

module.exports = router;
